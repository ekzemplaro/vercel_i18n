module.exports = {
  i18n: {
    defaultLocale: "en",
    locales: ["en", "sv", "zh-CN", "ja"],
    localePath: "./locales",
  },
};
